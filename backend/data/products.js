const products = [
	{
		
		name: 'Red Printed T-Shirt',
		image: '/images/product-1.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Burberry',
		category: 'Men Fashion',
		price: 1200,
		countInStock: 22,
		rating: 2.6,
		numReviews: 4,
	},
	{
		
		name: 'Black Shoes',
		image: '/images/product-2.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'HRX ',
		category: 'Men Fashion',
		price: 8500,
		countInStock: 42,
		rating: 1.5,
		numReviews: 2,
	},
	{
	
		name: 'Gray TrackPants',
		image: '/images/product-3.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Ralph Lauren',
		category: 'Men Fashion',
		price: 1000,
		countInStock: 18,
		rating: 4.5,
		numReviews: 2,
	},
	{
		
		name: 'Blue Printed Tshirt',
		image: '/images/product-4.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Gucci',
		category: 'Men Fashion',
		price: 1200,
		countInStock: 8,
		rating: 4.8,
		numReviews: 12,
	},
	{
		
		name: 'Gray Shoes',
		image: '/images/product-5.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Bata',
		category: 'Men Fashion',
		price: 4300,
		countInStock: 6,
		rating: 4.9,
		numReviews: 8,
	},
	{
	
		name: 'Black Puma T-shirt',
		image: '/images/product-6.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Tom Ford',
		category: 'Men Fashion',
		price: 1600,
		countInStock: 21,
		rating: 4.1,
		numReviews: 3,
	},
	{
		
		name: 'HRX Set of 3 Shocks',
		image: '/images/product-7.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'HRX',
		category: 'Men Fashion',
		price: 1500,
		countInStock: 25,
		rating: 3.4,
		numReviews: 3,
	},
	{
		
		name: 'Black Fossil Watch',
		image: '/images/product-8.jpg',
		description:
			'Etiam cursus condimentum vulputate. Nulla nisi orci, vulputate at dolor et, malesuada ultrices nisi. Ut varius ex ut purus porttitor, a facilisis orci condimentum. Nullam in elit et sapien ornare pellentesque at ac lorem.',
		brand: 'Fossil',
		category: 'Unisex Fashion',
		price: 9500,
		countInStock: 0,
		rating: 4.2,
		numReviews: 5,
	},
];

export default products;
// export deafault products;
//module.exports = products;
